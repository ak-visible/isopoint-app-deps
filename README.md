# Isopoint App Dependencies

This repository includes dependencies required to build the Isopoint distribution, but which are not required for development.  Keeping these dependencies in a separate repository saves time cloning the development repository, and also saves disk space.
